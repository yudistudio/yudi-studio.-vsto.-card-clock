﻿
namespace YudiStudio.VSTO.CardClock
{
	partial class Ribbon_CardClock : Microsoft.Office.Tools.Ribbon.RibbonBase
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Ribbon_CardClock()
			: base(Globals.Factory.GetRibbonFactory())
		{
			InitializeComponent();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 组件设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.tab1 = this.Factory.CreateRibbonTab();
			this.Group_CardClock = this.Factory.CreateRibbonGroup();
			this.DropDown_CardClock_Select = this.Factory.CreateRibbonDropDown();
			this.tab1.SuspendLayout();
			this.Group_CardClock.SuspendLayout();
			this.SuspendLayout();
			// 
			// tab1
			// 
			this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
			this.tab1.Groups.Add(this.Group_CardClock);
			this.tab1.Label = "TabAddIns";
			this.tab1.Name = "tab1";
			// 
			// Group_CardClock
			// 
			this.Group_CardClock.Items.Add(this.DropDown_CardClock_Select);
			this.Group_CardClock.Label = "卡机";
			this.Group_CardClock.Name = "Group_CardClock";
			// 
			// DropDown_CardClock_Select
			// 
			this.DropDown_CardClock_Select.Label = "读取卡机数据";
			this.DropDown_CardClock_Select.Name = "DropDown_CardClock_Select";
			// 
			// Ribbon_CardClock
			// 
			this.Name = "Ribbon_CardClock";
			this.RibbonType = "Microsoft.Excel.Workbook";
			this.Tabs.Add(this.tab1);
			this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_CardClock_Load);
			this.tab1.ResumeLayout(false);
			this.tab1.PerformLayout();
			this.Group_CardClock.ResumeLayout(false);
			this.Group_CardClock.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
		internal Microsoft.Office.Tools.Ribbon.RibbonGroup Group_CardClock;
		internal Microsoft.Office.Tools.Ribbon.RibbonDropDown DropDown_CardClock_Select;
	}

	partial class ThisRibbonCollection
	{
		internal Ribbon_CardClock Ribbon_CardClock
		{
			get { return this.GetRibbon<Ribbon_CardClock>(); }
		}
	}
}
