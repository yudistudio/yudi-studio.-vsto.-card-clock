﻿using System;
using System.Collections.Generic;

using Microsoft.Office.Tools.Ribbon;

namespace YudiStudio.VSTO.CardClock
{
	public partial class Ribbon_CardClock
	{
		public static string LastErrorInfo { get; set; }
		public static List<Entities.CardClockTcpInfo> CardClockTcpInfos { get; set; }
		private void Ribbon_CardClock_Load(object sender, RibbonUIEventArgs e)
		{
			this.DropDown_CardClock_Select.ItemsLoading += this.DropDown_CardClock_Select_ItemsLoading;
			this.DropDown_CardClock_Select.SelectionChanged += this.DropDown_CardClock_Select_SelectionChanged;
			CardClockTcpInfos = Utilities.CardClockHelper.GetCardClockTcpInfos();
		}

		private void DropDown_CardClock_Select_SelectionChanged(object sender, RibbonControlEventArgs e)
		{
			switch (DropDown_CardClock_Select.SelectedItem.Tag)
			{
				case "CradClockInfo":
					new Form_CardClockInfo().ShowDialog();
					break;
				case "default":
					break;
				default:
					var app = Globals.ThisAddIn.Application;
					Utilities.CardClockHelper.ReadData(DropDown_CardClock_Select.SelectedItem.Tag.ToString(), app);
					break;
			}
		}

		public void DropDown_CardClock_Select_ItemsLoading(object sender, RibbonControlEventArgs e) => this.ItemsLoading();

		public void ItemsLoading()
		{
			try
			{
				DropDown_CardClock_Select.Items.Clear();
				RibbonDropDownItem downItemDefault = this.Factory.CreateRibbonDropDownItem();
				downItemDefault.Tag = "default";
				downItemDefault.Label = "";
				DropDown_CardClock_Select.Items.Add(downItemDefault);
				RibbonDropDownItem downItemCradClockInfo = this.Factory.CreateRibbonDropDownItem();
				downItemCradClockInfo.Tag = "CradClockInfo";
				downItemCradClockInfo.Label = "卡机管理";
				DropDown_CardClock_Select.Items.Add(downItemCradClockInfo);
				for (int intCount = 0; intCount < CardClockTcpInfos?.Count; intCount++)
				{
					RibbonDropDownItem downItem = this.Factory.CreateRibbonDropDownItem();
					downItem.Tag = CardClockTcpInfos[intCount].ClockId;
					downItem.Label = CardClockTcpInfos[intCount].Name;
					DropDown_CardClock_Select.Items.Add(downItem);
				}
			}
			catch (Exception ex)
			{
				LastErrorInfo = ex.ToString();
			}
		}
	}
}
