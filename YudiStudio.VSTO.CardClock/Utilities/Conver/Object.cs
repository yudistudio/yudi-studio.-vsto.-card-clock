﻿using System;

namespace YudiStudio.VSTO.CardClock.Utilities.Conver
{
	public class Object
	{
		public static string LastErrorInfo { get; set; }

		/// <summary>
		/// 把对象转换为JSON字符串
		/// </summary>
		/// <param name="o">对象</param>
		/// <returns>JSON字符串</returns>
		public static string ToJson(object obj)
		{
			try
			{
				return System.Text.Json.JsonSerializer.Serialize(obj, new System.Text.Json.JsonSerializerOptions
				{
					ReadCommentHandling = System.Text.Json.JsonCommentHandling.Skip,
					AllowTrailingCommas = true,
					PropertyNamingPolicy = System.Text.Json.JsonNamingPolicy.CamelCase,
					WriteIndented = true
				});
			}
			catch (Exception ex)
			{
				LastErrorInfo = ex.Message;
				return "";
			}
		}
	}
}
