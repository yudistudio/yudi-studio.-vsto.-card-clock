﻿using System;
using System.IO;
using System.Text;

namespace YudiStudio.VSTO.CardClock.Utilities
{
	public class FileHelper
	{
		public static string LastErrorInfo { get; set; }
		public static string TextRead(string path)
		{
			var ReturnValue = string.Empty;
			try
			{
				if (File.Exists(path))
				{
					using var stream = new StreamReader(path, Encoding.UTF8);
					ReturnValue= stream.ReadToEnd();
				}					
			}
			catch(Exception ex)
			{
				LastErrorInfo = ex.ToString();
			}
			return ReturnValue;
		}

		public static void TextWrite(string path, string content)
		{
			try
			{
				using var stream = new StreamWriter(path, false, Encoding.UTF8);
				stream.WriteLine(content);
			}
			catch(Exception ex)
			{
				LastErrorInfo = ex.ToString();
			}
			
		}
	}
}
