﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace YudiStudio.VSTO.CardClock
{
	public partial class Form_CardClockInfo : Form
	{
		public Form_CardClockInfo()
		{
			InitializeComponent();
			this.DataGridView_CardClockInfo.RowPostPaint += this.DataGridView_CardClockInfo_RowPostPaint;
			this.DataGridView_CardClockInfo.CellClick += this.DataGridView_CardClockInfo_CellClick;
			this.Load += this.Form_CardClockInfo_Load;
			this.TMenu_ClockInfo_CurrentRowDel.Click += this.TMenu_ClockInfo_CurrentRowDel_Click;
			this.TMenu_ClockInfo_Save.Click += this.TMenu_ClockInfo_Save_Click;
		}

		private void DataGridView_CardClockInfo_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.ColumnIndex is 2)
			{
				Library.Windows.FormControls.DataGridViewComboBox((DataGridView)sender, Entities.ClockBrand.Brand, "", "", this.Cob_ClockBrand_TextChanged);
			}
		}

		private void Cob_ClockBrand_TextChanged(object sender, EventArgs e) => this.DataGridView_CardClockInfo.CurrentCell.Value = ((ComboBox)sender).Text;

		private void DataGridView_CardClockInfo_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e) => Library.Windows.FormControls.GridViewRowNumber((DataGridView)sender, e);

		private void TMenu_ClockInfo_Save_Click(object sender, EventArgs e)
		{
			var lstClockInfo = new List<Entities.CardClockTcpInfo>();
			for (int intCount = 0; intCount < DataGridView_CardClockInfo.RowCount - 1; intCount++)
			{
				lstClockInfo.Add(new Entities.CardClockTcpInfo()
				{
					ClockId = DataGridView_CardClockInfo.Rows[intCount].Cells[0].Value.ToString(),
					Name = DataGridView_CardClockInfo.Rows[intCount].Cells[1].Value.ToString(),
					Brand = DataGridView_CardClockInfo.Rows[intCount].Cells[2].Value.ToString(),
					IPAddress = DataGridView_CardClockInfo.Rows[intCount].Cells[3].Value.ToString(),
					Port = DataGridView_CardClockInfo.Rows[intCount].Cells[4].Value.ToString()
				});
			}
			Utilities.CardClockHelper.SetCardClockTcpInfo(lstClockInfo);
			Ribbon_CardClock.CardClockTcpInfos = lstClockInfo;
			MessageBox.Show("保存成功");
			this.Close();
		}

		private void TMenu_ClockInfo_CurrentRowDel_Click(object sender, EventArgs e)
		{
			if (DataGridView_CardClockInfo.CurrentRow != null)
				DataGridView_CardClockInfo.Rows.Remove(DataGridView_CardClockInfo.CurrentRow);
		}

		private void Form_CardClockInfo_Load(object sender, EventArgs e)
		{
			var cardClockInfo = Utilities.CardClockHelper.GetCardClockTcpInfos();
			for(int intCount = 0; intCount < cardClockInfo?.Count; intCount++)
			{
				int intRow = DataGridView_CardClockInfo.Rows.Add(new DataGridViewRow());
				DataGridView_CardClockInfo.Rows[intRow].Cells[0].Value = cardClockInfo[intCount].ClockId;
				DataGridView_CardClockInfo.Rows[intRow].Cells[1].Value = cardClockInfo[intCount].Name;
				DataGridView_CardClockInfo.Rows[intRow].Cells[2].Value = cardClockInfo[intCount].Brand;
				DataGridView_CardClockInfo.Rows[intRow].Cells[3].Value = cardClockInfo[intCount].IPAddress;
				DataGridView_CardClockInfo.Rows[intRow].Cells[4].Value = cardClockInfo[intCount].Port;
			}
		}
	}
}
