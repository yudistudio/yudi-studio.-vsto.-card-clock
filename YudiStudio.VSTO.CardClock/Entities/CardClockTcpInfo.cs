﻿namespace YudiStudio.VSTO.CardClock.Entities
{
	public class CardClockTcpInfo
	{
		public string ClockId { get; set; }
		public string Name { get; set; }
		public string Brand { get; set; }
		public string IPAddress { get; set; }
		public string Port { get; set; }
	}
}
