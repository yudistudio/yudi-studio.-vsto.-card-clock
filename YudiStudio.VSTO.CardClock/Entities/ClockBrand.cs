﻿using System.Collections.Generic;

namespace YudiStudio.VSTO.CardClock.Entities
{
	public class ClockBrand
	{
		public static List<string> Brand { get; set; } = new(new string[] { "依时利", "中控" });
	}
}
