﻿
namespace YudiStudio.VSTO.CardClock
{
	partial class Form_CardClockInfo
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.DataGridView_CardClockInfo = new System.Windows.Forms.DataGridView();
			this.CMenu_CardClockInfo = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.TMenu_ClockInfo_Save = new System.Windows.Forms.ToolStripMenuItem();
			this.TMenu_ClockInfo_CurrentRowDel = new System.Windows.Forms.ToolStripMenuItem();
			this.ClockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ClockName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Brand = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.IPAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ClockPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.DataGridView_CardClockInfo)).BeginInit();
			this.CMenu_CardClockInfo.SuspendLayout();
			this.SuspendLayout();
			// 
			// DataGridView_CardClockInfo
			// 
			this.DataGridView_CardClockInfo.AllowUserToOrderColumns = true;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			this.DataGridView_CardClockInfo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.DataGridView_CardClockInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.DataGridView_CardClockInfo.BackgroundColor = System.Drawing.SystemColors.Control;
			this.DataGridView_CardClockInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DataGridView_CardClockInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClockId,
            this.ClockName,
            this.Brand,
            this.IPAddress,
            this.ClockPort});
			this.DataGridView_CardClockInfo.ContextMenuStrip = this.CMenu_CardClockInfo;
			this.DataGridView_CardClockInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DataGridView_CardClockInfo.Location = new System.Drawing.Point(0, 0);
			this.DataGridView_CardClockInfo.Name = "DataGridView_CardClockInfo";
			this.DataGridView_CardClockInfo.RowTemplate.Height = 23;
			this.DataGridView_CardClockInfo.Size = new System.Drawing.Size(784, 561);
			this.DataGridView_CardClockInfo.TabIndex = 0;
			// 
			// CMenu_CardClockInfo
			// 
			this.CMenu_CardClockInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TMenu_ClockInfo_Save,
            this.TMenu_ClockInfo_CurrentRowDel});
			this.CMenu_CardClockInfo.Name = "CMenu_CardClockInfo";
			this.CMenu_CardClockInfo.Size = new System.Drawing.Size(137, 48);
			// 
			// TMenu_ClockInfo_Save
			// 
			this.TMenu_ClockInfo_Save.Name = "TMenu_ClockInfo_Save";
			this.TMenu_ClockInfo_Save.Size = new System.Drawing.Size(136, 22);
			this.TMenu_ClockInfo_Save.Text = "保存";
			// 
			// TMenu_ClockInfo_CurrentRowDel
			// 
			this.TMenu_ClockInfo_CurrentRowDel.Name = "TMenu_ClockInfo_CurrentRowDel";
			this.TMenu_ClockInfo_CurrentRowDel.Size = new System.Drawing.Size(136, 22);
			this.TMenu_ClockInfo_CurrentRowDel.Text = "删除当前行";
			// 
			// ClockId
			// 
			this.ClockId.HeaderText = "卡机编号";
			this.ClockId.Name = "ClockId";
			this.ClockId.Width = 81;
			// 
			// ClockName
			// 
			this.ClockName.HeaderText = "卡机名称";
			this.ClockName.Name = "ClockName";
			this.ClockName.ToolTipText = "这里设置你可以记住的名称";
			this.ClockName.Width = 81;
			// 
			// Brand
			// 
			this.Brand.HeaderText = "卡机品牌";
			this.Brand.Name = "Brand";
			this.Brand.Width = 81;
			// 
			// IPAddress
			// 
			this.IPAddress.HeaderText = "卡机地址";
			this.IPAddress.Name = "IPAddress";
			this.IPAddress.Width = 81;
			// 
			// ClockPort
			// 
			this.ClockPort.HeaderText = "卡机端口";
			this.ClockPort.Name = "ClockPort";
			this.ClockPort.Width = 81;
			// 
			// Form_CardClockInfo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.DataGridView_CardClockInfo);
			this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "Form_CardClockInfo";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "卡机管理";
			((System.ComponentModel.ISupportInitialize)(this.DataGridView_CardClockInfo)).EndInit();
			this.CMenu_CardClockInfo.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView DataGridView_CardClockInfo;
		private System.Windows.Forms.ContextMenuStrip CMenu_CardClockInfo;
		private System.Windows.Forms.ToolStripMenuItem TMenu_ClockInfo_Save;
		private System.Windows.Forms.ToolStripMenuItem TMenu_ClockInfo_CurrentRowDel;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClockId;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClockName;
		private System.Windows.Forms.DataGridViewTextBoxColumn Brand;
		private System.Windows.Forms.DataGridViewTextBoxColumn IPAddress;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClockPort;
	}
}